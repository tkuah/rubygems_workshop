# frozen_string_literal: true

module Workshop
  class Translator
    def initialize(language)
      @language = language
    end

    def hi
      case @language
      when "en"
        "Hello!"
      when "es"
        "Hola!"
      else
        "<Un-translated>"
      end
    end
  end
end
