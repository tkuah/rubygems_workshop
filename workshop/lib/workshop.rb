# frozen_string_literal: true

require_relative "workshop/version"
require "workshop/translator"

module Workshop
  class Error < StandardError; end

  def self.hi(language = "en")
    translator = Translator.new(language)
    translator.hi
  end
end
