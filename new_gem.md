# Creating a new gem

This part shows you how to create a new gem.

## Make sure you have bundler

Ensure you also have `bundler` installed:

```sh
gem install bundler
```

## Naming conventions

Before creating a new gem, we need to find a name.

<https://guides.rubygems.org/patterns/#consistent-naming> and <https://guides.rubygems.org/name-your-gem/>
have some recommendations related to gem naming conventions.

If you wish to publish your gem later (not covered in this workshop), it is
helpful to ensure your gem name is unique.

## Creating a new gem

Choose a new name for your gem. For this workshop, we will use the name `workshop`.
To create a new gem, type:

```sh
bundle gem workshop --test=rspec --ci=gitlab
```

You may need to answer with `y`, or `n` to a few options.

If this command was succesful, you will see that a new directory `workshop` was created
for you, with some files inside the directory.

## Understand the directories and files inside a gem

Go inside the directory that was created:

```
cd workshop
```

There were many files and directories created. The three places we will focus on are:

- `lib/workshop.rb`. This is the main file that will be loaded when your gem is loaded.
  Note that the name of the file here should match the name of your gem.
- `workshop.gemspec`. This file is where you put various specifications for your gem like
  gem author, gem license, and also gem dependencies.
- `lib/workshop`. This directory and `lib/workshop.rb` contains all the code for your gem.
  If you need extra classes, or files for your gem's code, this directory is
  where put these files.

This is a good time to commit the generated files into git:

```sh
git commit -m "Initial commit"
```

## Push to GitLab

This will also be a good time to push your code to GitLab.

1. Follow the instructions to [create a new project](https://docs.gitlab.com/ee/user/project/#create-a-blank-project) on GitLab.
   Un-select the `Initialize repository with a README` option before creating
   the new project on GitLab.com.
1. Follow the instructions on the project page to push the code to GitLab.

After you have pushed your code to GitLab, you should notice that GitLab has
started a pipeline to run tests for your gem.
The pipeline might fail, but that is OK.
We will fix the failure soon.

## Add a simple new feature

We will write a simple new feature: the Gem will say "Hello World!" in your language.

Open the `lib/workshop.rb` file in your favourite editor.

You will see that the file contains code similar to:

```ruby
# frozen_string_literal: true

require_relative "workshop/version"

module Workshop
  class Error < StandardError; end
  # Your code goes here...
end
```

Modify that code to add our method to say hello:

```ruby
# frozen_string_literal: true

require_relative "workshop/version"

module Workshop
  class Error < StandardError; end

  def self.hi
    "Hello!"
  end
end
```

❓ Stuck ? Ask a workshop staff for help.

This will also be a good time to commit and push the code into git:

```sh
git add lib/workshop.rb
git commit -m "Added ability to say hi"
git push
```

## Installing the gem from a local build

Let's test out the functionality we have just written.
To do so, we will need to install the gem that we just wrote.

This section guides you on how to do that.

❓ Stuck ? Ask a workshop staff for help.

First, we need to fix the `workshop.gemspec` to fill in all the `TODO` items:

- Set `spec.summary` to `"Rubygems workshop"`
- Set `spec.description` to `"Rubygems workshop"`
- Set `spec.homepage` to `"https://gitlab.com"` for now.
- Set `spec.allowed_push_host` to `"https://gitlab.com"`
- Set `spec.source_code_uri` to `"https://gitlab.com"` for now.
- Set `spec.changelog_uri` to `"https://gitlab.com"` for now.

Next, we can now build the gem. Run the following command on the terminal:

```sh
gem build
```

You should see output similar to:

```
  Successfully built RubyGem
  Name: workshop
  Version: 0.1.0
  File: workshop-0.1.0.gem
```

Finally, we can now install the gem with this command:

```sh
gem install workshop-0.1.0.gem
```

Now that the gem is successfully installed, we can try to use it.

Open `irb`:

```sh
irb
```

This will open a Ruby intepreter. Type the following Ruby code:

```ruby
require 'workshop'
Workshop.hi
```

This should print the message that you programmed earlier !

This will also be a good time to commit the code into git:

```sh
git add workshop.gemspec
git commit -m "Fixed gemspec so we can build gem"
git push
```

## Write a test for the new feature

Before we write tests, we need to install a dependency, `rspec`.
To do run so, run the following command:

```sh
bundle install
```

Let's write a test for the new code we have written.
The generator should have generated some tests for us in the file
`spec/workshop_spec.rb`.

Open the `spec/workshop_spec.rb` file in your favourite editor.

A portion of the file will look something like this:

```ruby
  it "does something useful" do
    expect(false).to eq(true)
  end
```

Replace that part with the following:

```ruby
  it "says hi" do
    expect(Workshop.hi).to eq("Hello!")
  end
```

Now we can run this spec to see if it is passing:

```sh
bundle exec rspec spec/workshop_spec.rb
```

This will also be a good time to commit the code into git:

```sh
git add spec/workshop_spec.rb
git commit -m "Added spec"
git push
```

## Requiring more files

❓ Stuck ? Ask a workshop staff for help.

Let us say we wish to add different languages to say "Hi".
Update the the code to `lib/workshop.rb` to be like this:

```ruby
# frozen_string_literal: true

require_relative "workshop/version"

module Workshop
  class Error < StandardError; end

  def self.hi(language = "en")
    translator = Translator.new(language)
    translator.hi
  end

  class Translator
    def initialize(language)
      @language = language
    end

    def hi
      case @language
      when "en"
        "Hello!"
      when "es"
        "Hola!"
      else
        "<Un-translated>"
      end
    end
  end
end
```

But this makes this file quite long now.
So, let us move the `Translator` class into a new file.

Create a new file in `lib/workshop/translator.rb` with the following contents:

```ruby
# frozen_string_literal: true

module Workshop
  class Translator
    def initialize(language)
      @language = language
    end

    def hi
      case @language
      when "en"
        "Hello!"
      when "es"
        "Hola!"
      else
        "<Un-translated>"
      end
    end
  end
end
```

Try running specs now. What error do you see ?

```sh
bundle exec rspec
```

You should see the following error:

```sh
     Failure/Error: translator = Translator.new(language)
     
     NameError:
       uninitialized constant Workshop::Translator
     
           translator = Translator.new(language)
                        ^^^^^^^^^^
```

This is because the new file `lib/workshop/translater.rb` needs to be loaded
from `lib/workshop.rb`.
We can do this by adding a `require` statement into `lib/workshop.rb`:

```ruby
# frozen_string_literal: true

require_relative "workshop/version"
require "workshop/translator"

module Workshop
  class Error < StandardError; end

  def self.hi(language = "en")
    translator = Translator.new(language)
    translator.hi
  end
end
```

Run specs again. The specs should run successfully now:

```sh
bundle exec rspec
```

This will also be a good time to commit and push the code into git:

```sh
git add lib/workshop.rb
git add lib/workshop/translator.rb
git commit -m "Added ability to say hi in another langugage"
git push
```

## Finish

Congratulations ! You have created your first gem.

If you wish to continue further, you can consider:

* Adding more languages, and tests
* Publishing your gem on https://rubygems.org.
  See the [publishing guide](https://guides.rubygems.org/publishing/)
* Add a dependency into your gem like, `i18n`.

## Cleanup

If you have installed the `workshop` gem, you can remove it by uninstall it
with:
```
gem uninstall workshop
```

## Credits

This is based on the <https://bundler.io/guides/creating_gem.html>, and <https://guides.rubygems.org/make-your-own-gem>.
