# RubyGems workshop

In this workshop, partiticipants will collaborate with each other to:

* create a new gem,
* understand the directories involved,
* write some simple new feature,
* and write some tests

For advanced participants, they could elect to: 

* extract functionality out from [monorepo into gems](https://gitlab.com/groups/gitlab-org/-/epics/10869).

## Prerequisites

You will require a computer with either:

* Linux operating system
* MacOS operating system

Windows operating system is acceptable but the workshop coaches
may not be as familar.
It may be helpful for this workshop to install WSL.

Familarity with Ruby, or similar programming languages is helpful.

## Setup

### Installing Ruby

It is recommended that you have at least Ruby 3.1 installed.
Ask the workshop coaches for help on how to install Ruby.

### Setup Git

At GitLab, we use Git for version control.

If you have Git installed, please setup your Git email and username:

```sh
git config --global user.name "<YOUR NAME>"
git config --global user.email "<YOUR_EMAIL>"
```

## Next step

Now, you can proceed to creating a [new gem](new_gem.md).
